(* $Id: fed8c32f7754bb9156f2d96f1f446bef2acc73f8 $ *)


(**
   Numerical Non-Relational Abstract Domains.

   A numerical non-relational abstract domain is an abstract lattice that is in
   Galois connection with the concrete lattice (℘(ℤ), ⊆).  The domain comes with
   abstract numerical operations, and is equipped with convergence acceleration
   operators (widening and narrowing).

   {L {b Implemented by:} {! DomConstant}, {! DomSign}, {! DomInterval},
      {! DomCongruence}.}
 *)


(**
   Common interface to all numerical non-relational abstract domains.
 *)

(**
  Underlying lattice (A, ⊑).
*)

type t = 
   | Top
   | Bot
   | Val of (Z.t * Z.t)

(**
  Least element ⊥.
*)

let print fmt a = 
  match a with
  | Bot -> Format.fprintf fmt "⊥"
  | Top -> Format.fprintf fmt "ℤ"
  | Val (r,m) -> 
    if (m = Z.zero) then Format.fprintf fmt "%s" (Z.to_string r)
    else
      if (r = Z.zero) then 
        if (m == Z.one) then Format.fprintf fmt "ℤ"
        else Format.fprintf fmt "%sℤ" (Z.to_string m)
      else
         Format.fprintf fmt "(%s + %sℤ)" (Z.to_string r) (Z.to_string m)

let bot = Bot
let top = Val (Z.zero, Z.one)

(**
  [equal a b] tests whether [a] and [b] are equal.
*)
let equal a b =
  match (a, b) with
  | (Top, Top)
  | (Bot, Bot) -> true
  | (Bot, _)
  | (_, Bot)
  | (Top, _)
  | (_, Top) -> false
  | (Val (ra, ma), Val (rb, mb)) -> (ra = rb) && (ma = mb)

(**
  [leq a b] tests whether [a] is lesser than or equal to [b].
*)
let leq a b = 
  match (a, b) with
  | (Top, Top)
  | (Bot, Bot)
  | (_  , Top)
  | (Bot, _  ) -> true
  | (Top, _  ) 
  | (_  , Bot) -> false 
  | (Val (ra, ma), Val (rb, mb)) ->
      if (mb = Z.zero) then (ra = rb && ma = Z.zero) (* Si b est un singleton *)
      else 
        if(ma = Z.zero) then ((Z.erem ra mb) = rb) (* Si a est un singleton *)
        else
          (((Z.erem ra mb) = rb) && (Z.erem ma mb) = Z.zero)
(**
  [glb a b] returns the greatest lower bound of [a] and [b].
*)
let glb a b =
  match (a, b) with
  | (Bot, _  )
  | (_  , Bot) -> Bot
  | (Top, _  ) -> b
  | (_  , Top) -> a
  | (Val (ra, ma), Val (rb, mb)) ->
    if ((Z.equal ma Z.zero) || (Z.equal mb Z.zero)) then (* Si a ou b est un singleton *)
      if (not (Z.equal ma Z.zero)) then (* Si b est le singleton *)
        if (Z.equal ra (Z.erem rb ma)) then Val (rb, Z.zero)
        else
          Bot
      else
        if (not (Z.equal mb Z.zero)) then (* Si b n'est pas un singleton *)
          if (Z.equal rb (Z.erem ra mb)) then Val (ra, Z.zero)
          else
            Bot
        else
          if (Z.equal ra rb) then Val (ra, Z.zero)
          else
            Bot
    else
      let diff = (Z.sub rb ra) in
      if (Z.divisible diff (Z.gcd ma mb)) then (* Nous vérifion le critère cité dans la thèse pour savoir si la borne inférieure est non vide *)
        match (Z.gcdext ma mb) with (g, x, y) ->
          let new_m = (Z.div (Z.mul mb ma) g) in
          let new_r = (Z.add (Z.mul (Z.div (Z.mul diff x) g) ma) ra) in
          Val (Z.erem new_r new_m, new_m)
      else
        Bot

(**
  [lub a b] returns the least upper bound of [a] and [b].
*)
let lub a b = 
   match (a, b) with
  | (Top, _  )
  | (_  , Top) -> Top
  | (Bot, _  ) -> b
  | (_  , Bot) -> a
  | (Val (ra, ma), Val (rb, mb)) ->
    let r_min = Z.min ra rb 
    and r_max = Z.max ra rb in
    let new_m = (Z.gcd (Z.gcd ma mb) (Z.sub r_max r_min)) in
    if (Z.equal new_m Z.zero) then (* Nous nous assurons de ne pas diviser r_min par new_m pour obtenir le nouveau reste si new_m = 0 *)
      Val (r_min, Z.zero)
    else
      Val (Z.erem r_min new_m, new_m)

(**
  Abstraction function α (for singletons).
*)
let abs v = Val ((Z.of_int v), Z.zero)

(**
  [empty a] tests whether the concretization of [a] is the empty set.
*)
let empty v = equal v Bot
(**
  Abstract numerical operations.
*)
module Op =
   struct 
 (**
    [add a b] returns the abstraction of
    \{x+y | x ∈ γ([a]) ∧ y ∈ γ([b])\}.
  *)
 let add a b =
  match (a, b) with
  | (Bot, _  )
  | (_  , Bot) -> Bot
  | (Top, _  )
  | (_  , Top) -> Top 
  | (Val (ra, ma) , Val (rb, mb)) ->
    if (ma = Z.zero && mb = Z.zero) (* Si a et b sont des singletons *)
    then
      Val ((Z.add ra rb) , Z.zero)
    else
      let new_m = (Z.gcd ma mb) in
      Val (Z.erem (Z.add ra rb) new_m , new_m)

 (**
    [sub a b] returns the abstraction of
    \{x-y | x ∈ γ([a]) ∧ y ∈ γ([b])\}.
  *)
 let sub a b =
  match (a, b) with
  | (Bot, _  )
  | (_  , Bot) -> Bot
  | (Top, _  )
  | (_  , Top) -> Top 
  | (Val (ra, ma) , Val (rb, mb)) ->
    if (ma = Z.zero && mb = Z.zero) (* Si a et b sont des singletons *)
    then
      Val ((Z.sub ra rb) , Z.zero)
    else
      let new_m = (Z.gcd ma mb) in
      Val (Z.erem (Z.sub ra rb) new_m , new_m)

 (**
    [mul a b] returns the abstraction of
    \{x*y | x ∈ γ([a]) ∧ y ∈ γ([b])\}.
  *)
 let mul a b =
  match (a, b) with
  | (Bot, _  )
  | (_  , Bot) -> Bot
  | (Top, _  )
  | (_  , Top) -> Top 
  | (Val (ra, ma) , Val (rb, mb)) ->
    if (ma = Z.zero && mb = Z.zero) (* Si a et b sont des singletons *)
    then
      Val ((Z.mul ra rb) , Z.zero)
    else
      let new_m = Z.gcd (Z.gcd (Z.mul ra mb) (Z.mul rb ma)) (Z.mul ma mb) in
      if (new_m = Z.zero) then Val ((Z.mul ra rb), Z.zero) (* Nous nous assurons de ne pas diviser new_r par new_m pour obtenir le nouveau reste si new_m = 0 *)
      else
        Val (Z.erem (Z.mul ra rb) new_m , new_m)

 (**
    [div a b] returns the abstraction of
    \{x/y | x é∈ γ([a]) ∧ y ∈ γ([b]) ∧ y≠0\}.
  *)
 let div a b =
  match (a, b) with
  | (Bot, _  )
  | (_  , Bot) -> Bot
  | (Val (ra, ma) , Val (rb, mb)) ->
    if (Z.equal rb Z.zero) && (Z.equal mb Z.zero) then (* Si le dénominateur est le singleton {0}, la division n'est pas permise *)
      Bot
    else
      if (Z.equal mb Z.zero) && (Z.equal ma Z.zero) then (* Si a et b sont deux singletons *)
        if (Z.equal rb Z.zero) then Bot (* Nous devons nous assurer de na pas diviser par 0 *)
        else
          Val (Z.div ra rb, Z.zero)
      else
        if (Z.equal mb Z.zero) then (* Si b est un singleton *)
          if (Z.divisible ma rb) && (Z.divisible ra rb) then Val (Z.div ra rb, Z.div ma rb) (* Nous vérifions si rb divise ra et ma *)
          else
            Top
        else
          if (Z.equal ma Z.zero) then (* Si a est un singleton *)
            let n = (Z.sub (Z.abs ra) (Z.erem (Z.sub (Z.abs ra) rb) mb)) in (* Ce calcul est celui présent dans la thèse concernant la division. Les valeurs absolues sont dues à l'astuce citée dans le rapport *)
            if (Z.leq n Z.zero) then
              Val (Z.zero, Z.zero)
            else
              Val (Z.zero, Z.abs (Z.div ra n))
          else
            Top

 (**
    [equality a b c] solves the “linear equality” [a]*x + [b] = 0 subject to
    the condition that x ⊑ [c].  It returns an abstract element that is
    greater than or equal to the abstraction of the set of solutions of the
    equality, namely the set
    S = \{x ∈ γ([c]) | ∃ r ∈ γ([a]), ∃ s ∈ γ([b]) : r*x + s = 0\}.
    Smaller abstract elements are better.  Ideally, the returned abstract
    element is α(S).
  *)
 let equality a b c =
  match (a, b, c) with
  | (Bot, _, _)
  | (_, Bot, _)
  | (_, _, Bot) -> Bot
  | (Val(ra,ma), Val(rb,mb), Val(rc,mc)) ->
    if (Z.equal ra Z.zero) && (Z.equal ma Z.zero) then (* Si a est {0}, alors il est nécessaire que b contienne 0, dans ce cas, x peut prendre toute valeur de c, sinon aucune valeur n'est possible *)
      if (leq (abs 0) b) then c
      else Bot
    else
      if (Z.equal mc Z.zero) then (* Si c est un singleton, alors il suffit de voir si 0 appartient au domaine de congruence a.c + b, dans ce cas, x peut incarner le singleton c, sinon c'est Bot *)
        if (leq (abs 0) (add b (mul a c))) then c
        else
          Bot
      else
        let rb = if (Z.equal mb Z.zero) then (Z.sub Z.zero rb) else (Z.erem (Z.sub mb rb) mb) in (* nous passons b de l'autre côté de l'égalité pour faire des résolutions d'équations diophantiennes *)
        let new_dom = (
          if (Z.equal ma Z.zero) then (* Si a est un singleton *)
            if (Z.equal mb Z.zero) then (* Et si b est aussi un singleton *)
              if (Z.divisible rb ra) then Val (Z.div rb ra, Z.zero) (* Alors nous vérifions que ra divise rb, si oui, alors x peut incarner le singleton {rb/ra}, sinon Bot *)
              else
                Bot
            else
              if (Z.divisible rb (Z.gcd ra mb)) then (* Ici, b n'est plus un singleton, comme a en est encore un, nous résolvons une équation diophantienne *)
                match (Z.gcdext ra mb) with (g, x, y) ->
                  let new_m = (Z.div mb g) in
                  let new_r = (Z.div (Z.mul rb x) g) in
                  Val (Z.erem new_r new_m, new_m)
              else
                Bot
          else
            let new_mod = (Z.gcd ma mb) in (* Si a n'est pas un singleton, l'astuce consiste à remarquer que l'on peut passer le modulo du domaine a de l'autre côté de l'équation, en prennant garde à conserver les valeurs dans b également, d'où le fait de prendre le pgcd de ma et mb *)
            if (Z.divisible rb (Z.gcd ra new_mod)) then (* Puis nous résolvons cette équation diophantienne *)
              match (Z.gcdext ra new_mod) with (g, x, y) ->
                let new_m = (Z.div new_mod g) in
                let new_r = (Z.div (Z.mul rb x) g) in
                Val (Z.erem new_r new_m, new_m)
            else
              Bot
        ) in
        (glb new_dom c) (* Enfin, nous ne prennons que les solutions appartenant au domaine c *)

 (**
    [inequality a b c] solves the “linear inequality” [a]*x + [b] ≤ 0 subject
    to the condition that x ⊑ [c].  It returns an abstract element that is
    greater than or equal to the abstraction of the set of solutions of the
    inequality, namely the set
    S = \{x ∈ γ([c]) | ∃ r ∈ γ([a]), ∃ s ∈ γ([b]) : r*x + s ≤ 0\}.
    Smaller abstract elements are better.  Ideally, the returned abstract
    element is α(S).
  *)
 let inequality a b c =
  match (a, b, c) with
  | (Bot, _, _)
  | (_, Bot, _)
  | (_, _, Bot) -> Bot
  | (Val(ra,ma), Val(rb,mb), Val(rc,mc)) ->
    if not (Z.equal mb Z.zero) then c (* Si b n'est pas un singleton, alors x peut incarnenr tout entier relatif dans c étant donné que l'on pourra toujours trouver une valeur dans le domaine b assez faible pour que le membre gauche de l'inégalité soit négatif ou nul *)
    else
      if (Z.equal mc Z.zero) then (* Si c est un singleton *)
        if (Z.equal ma Z.zero) then (* Et si a est aussi un singleton *)
          if (Z.leq (Z.add rb (Z.mul ra rc)) Z.zero) then c (* Alors, comme b est aussi un singleton ici, nous vérifions que c vérifie l'inéquation a.c + b <= 0 *)
          else
            Bot
        else
          if (Z.equal rc Z.zero) then (* Si c est le singleton {0} *)
            if (Z.leq rb Z.zero) then c (* Alors x pourra incarner c uniquement si b (qui est ici un singleton également) est un singleton de la forme {m} avec m <= 0 *)
            else
              Bot
          else
            c (* Dans ce cas restant, comme a n'est pas un singleton, nous sommes assurés que quelque soit la valeur du singleton c et celle de b, il existera une valeur v dans le domaine a du signe opposé à celle de c et assez grande pour que v.c + b <= 0 *)
      else
        if (Z.equal ma Z.zero) then (* Si a est un singleton *)
          if (Z.equal ra Z.zero) then (* Et si a est précisément le singleton {0} *)
            if (Z.leq rb Z.zero) then c (* Alors x pourra incarner les valeurs dans c uniquement si b (qui est ici un singleton également) est un singleton de la forme {m} avec m <= 0 *)
            else
              Bot
          else (* Si a est un singleton différent de {0}, alors il existe une partie du domaine de c vérifiant l'inéquation. Cependant, il n'est pas possible de désigner exactement ces valeurs avec un domaine de congruence (elles admettent une borne inférieure ou supérieure selon le signe su singleton a). Nous donnons donc une approximation en donnant c *)
            c
        else
          c (* Dans ce cas restant, comme a n'est pas un singleton, nous sommes assurés que quelque soit la valeur v' prise dans le domaine c et la valeur de b, il existera une valeur v dans le domaine a du signe opposé à celle de c et assez grande pour que v.v' + b <= 0*)

end

(**
  [widen a b] returns the widening of [a] with [b] (i.e., [a] ∇ [b]).
  {L {b Ensures:} [a] ⊑ ([widen a b]) and [b] ⊑ ([widen a b]).}
*)
let widen v1 v2 = (lub v1 v2)

(**
  [narrow a b] returns the narrowing of [a] with [b] (i.e., [a] ∆ [b]).
  {L {b Requires:} [b] ⊑ [a].}
  {L {b Ensures:} [b] ⊑ ([narrow a b]) ⊑ [a].}
*)
let narrow v1 v2 = v2